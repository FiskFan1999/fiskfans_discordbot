import discord
from dotenv import load_dotenv
import asyncio
import json
import random
import listadmins

PATH = "./safe/funfacts.json"

template = {"fact": "fact goes here",
        "img": "image here (direct url only)",
        "source": "source url here"
        }

def get_json(path):
    with open(path) as json_file:
        file_json = json.load(json_file)
    return file_json

def write_json(json_stuff, path):
    with open(path, "w") as json_file:
        json.dump(json_stuff, json_file)
    return

prompt = ".organfact"

async def printfact(client, message, fact):
    if fact["source"]:
        emb = discord.Embed(title=fact["fact"], url=fact["source"])
    else:
        emb = discord.Embed(title=fact["fact"])

    if fact["img"] != "":
        emb.set_image(url=fact["img"])

    await message.channel.send(embed=emb)
async def dmfact(client, message, fact):
    if fact["source"]:
        emb = discord.Embed(title=fact["fact"], url=fact["source"])
    else:
        emb = discord.Embed(title=fact["fact"])

    if fact["img"] != "":
        emb.set_image(url=fact["img"])

    await message.author.send(embed=emb)

facts_regular = get_json(PATH)

facts_len = len(facts_regular)

async def getfact(client, message):
    global facts_regular
    mes = message.content.lower()
    if mes == prompt + " all" and listadmins.is_user_admin(message):
        await message.channel.send("The bot is about to dm you all " + str(facts_len) + " fun facts. Are you sure about this? If so, type `.organfact all " + str(facts_len) + "`.")
        return
    if mes == prompt + " all " + str(facts_len) and listadmins.is_user_admin(message):
        all_facts = get_json(PATH)
        for fact in all_facts:
            await dmfact(client, message, fact)
    elif len(mes.split()) == 2 and await listadmins.is_user_admin(message):
        all_facts = get_json(PATH)
        fact = all_facts[int(mes.split()[1])]
        await printfact(client, message, fact)

    else:
        fact_index = random.randrange(0, len(facts_regular))
        fact = facts_regular.pop(fact_index)
        print("len(facts_regular)=", len(facts_regular))

        if len(facts_regular) == 0:
            facts_regular = get_json(PATH)

        await printfact(client, message, fact)


