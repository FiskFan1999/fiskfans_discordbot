import discord
import requests
import xml.etree.ElementTree as ET

import maketextfile

prompt = ".npor"

def getRegularUrl(idstr):
    s = "https://www.npor.org.uk/NPORView.html?RI=" + str(idstr)
    return s

def getXmlPageUrl(idstr):
    s = "https://www.npor.org.uk/cgi-bin/XMLFunctions.cgi?Fn=XMLGetSurvey&Rec_index=" + str(idstr)
    return s

def getXmlText(idstr):
    req = requests.get(getXmlPageUrl(idstr))
    return req.text

def getImageUrl(idstr):
    s = "https://www.npor.org.uk/cgi-bin/XMLFunctions.cgi?Fn=GetPicture&Rec_index=" + str(idstr) + "&Number=1"
    return s

def getRoot(text):
    root = ET.fromstring(text)
    return root

def getAddressString(di):
    #di = dictionary from building tag
    s = ""
    terms = [
            "name",
            "type",
            "address",
            "suburb",
            "town",
            "county"
            ]
    for t in terms:
        if t in di and len(di[t].strip()):
            s += di[t]
            if t != "name" and t != terms[-1]:
                s += ","
            s += " "
    return s

def getDivisionString(divisionRoot):
    #given a "Department" root, which contains a number of "Stop" childs,
    #Format the string containing the stops.
    s = ""
    divisionName = divisionRoot.attrib["name"]
    s += "**" + divisionName + "**"
    if "enclosed" in divisionRoot.attrib and divisionRoot.attrib["enclosed"].lower()[0:1] == "y":
        s += " (enclosed)"

    s += "\n"

    for x in divisionRoot:
        if "name" in x.attrib and "pitch" in x.attrib:
            s += str(x.attrib["pitch"]) + " " + x.attrib["name"] + "\n"

    return s

def getBuilderString(root):
    print(root.attrib)
    print(root.text)

    s = "**"
    #{'address': 'Ruardean', 'date': '1974', 'firm_ref': '', 'name': 'J. Coulson'}
    if "name" in root.attrib:
        s += root.attrib["name"] + " "

    if "date" in root.attrib:
        s += root.attrib["date"]

    s += "**\n"
    t = ""
    if root.text:
        t = root.text.replace("\n", " ").strip()
    if t:
        s += t
    return s




def getAllInformation(root):
    allInformation = dict()
    allInformation["builders"] = []
    allInformation["divisions"] = []

    for x in root[0]:
        if x.tag == "Building":
            allInformation["address"] = getAddressString(x.attrib).strip()
        elif x.tag == "Properties":
            allInformation["properties"] = x.attrib
        elif x.tag == "Builder":
            allInformation["builders"].append(getBuilderString(x))
        elif x.tag == "Department":
            #Division (stoplist)
            allInformation["divisions"].append(getDivisionString(x))

        else:
            pass
            print(x.tag, x.attrib)

    return allInformation

def getSearchUrl(terms):
    s = "https://www.npor.org.uk/cgi-bin/XMLFunctions.cgi?Fn=KeySearch&Keyword=" + terms
    return s

def listSearchResults(root):
    s = ""
    for r in root:
        if r.tag != "Building":
            continue

        s += "**" + getAddressString(r.attrib) + "**"
        rec_index = None
        for child in r:
            if child.tag == "Survey":
                for c2 in child:
                    if "rec_index" in c2.attrib:
                        s += "\n__" + c2.attrib["rec_index"] + "__:"

                    elif c2.tag == "Builder" and c2.attrib["name"].strip() and c2.attrib["date"].strip():
                        s += " " + c2.attrib["name"] + " " + str(c2.attrib["date"]) + ", "
                    
        s += "\n"


    return s


async def run(client, message):
    argv = message.content.strip().split()

    if len(argv) > 2 and (argv[1] == "search" or argv[1] == "q"):
        terms = "+".join(argv[2:])
        url = getSearchUrl(terms)
        req = requests.get(url)
        text = req.text
        root = getRoot(text)
        embed = discord.Embed(title="Search results", description = listSearchResults(root)[:2048])
        await message.channel.send(embed=embed)
        return

    if len(argv) != 2:
        await message.channel.send(".npor [id]\n.npor search [keywords]")
        return

    idstr = argv[1]

    xmltext = (getXmlText(idstr))
    root = getRoot(xmltext)
    #print(getAllInformation(root))
    allInformation = getAllInformation(root)
    description = (getEmbedDescriptionText(allInformation))

    if len(description) > 1999:
        f = maketextfile.maketextfile(description.replace("*", ""))
        embed = discord.Embed(title=allInformation["address"], url=getRegularUrl(idstr))
        embed.set_image(url=getImageUrl(idstr))
        await message.channel.send(file=f,embed=embed)
        return


    embed = discord.Embed(title=allInformation["address"], description=description[:2048], url=getRegularUrl(idstr))
    embed.set_image(url=getImageUrl(idstr))
    await message.channel.send(embed=embed)
    
def getEmbedDescriptionText(allInfo):
    s = ""

    #s += allInfo["address"] + "\n\n"

    for build in allInfo["builders"]:
        s += build
        s += "\n\n"

    for d in allInfo["divisions"]:
        s += d
        s += "\n"

    return s

def testingmain():
    testingID = "N00059"
    xmltext = (getXmlText(testingID))
    root = getRoot(xmltext)
    #print(getAllInformation(root))
    allInformation = getAllInformation(root)
    print(getEmbedDescriptionText(allInformation))

if __name__ == "__main__":
    testingmain()
