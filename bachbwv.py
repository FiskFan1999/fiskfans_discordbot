import asyncio
import subprocess
import discord

def getBWVSearch(idstr):
    cmd = ["./bwvsearchscript", idstr]
    result = subprocess.run(cmd, stdout=subprocess.PIPE)
    return result.stdout.decode("utf-8").strip()

bachPrompt = ".bwv"

async def bwvSearch(client, message):
    argv = message.content.split()
    if len(argv) == 1:
        await message.channel.send("*.bwv <number>*")
        return

    if len(argv) == 2:
        idstr = argv[1]
        results = getBWVSearch(idstr)
        if len(results) == 0:
            await message.channel.send("No bwv number found")
            return
        emb = discord.Embed(description="**" + results[:1996] + "**")
        await message.channel.send(embed=emb)


