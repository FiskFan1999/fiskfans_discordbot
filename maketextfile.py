import discord

def maketextfile(text):
    #text is the string to write to the file
    #will return a discord.File that should be put in the message
    #(file=maketextfile(text))
    FILENAME = "./text.txt"
    f = open(FILENAME, "w")
    f.write(text)
    f.close()
    return discord.File(FILENAME)
