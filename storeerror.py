import datetime
DIRPATH = "./errors/"

def storeerror(s, message):
    filename = DIRPATH + str(datetime.datetime.now())
    with open(filename, "w") as f:
        f.write(message.content)
        f.write("\n")
        f.write("\n")
        f.write(s)

