import json
import os
import random
import discord
import requests
from dotenv import load_dotenv
import datetime
import listadmins

FILEPATH = "./safe/facades2.json"

lastcommand = datetime.datetime.now()
print(lastcommand)
cooldown_seconds = 2

ROMAN_NUMERALS = [
        "",
        "I",
        "II",
        "III",
        "IV",
        "V",
        "VI",
        "VII"
        ]
def get_roman_numeral(i):
    if i < 0 or i >= len(ROMAN_NUMERALS):
        return str(i)
    else:
        return ROMAN_NUMERALS[i]

def get_correction_link(idnum, organ):
    return "https://docs.google.com/forms/d/e/<redacted>/viewform?usp=pp_url&entry.512163920=" + str(idnum) + "&entry.1770441750=" + organ["name"].replace(" ", "+")

async def get_facade(client, message):
    global lastcommand
    now = datetime.datetime.now()
    difference = now - lastcommand
    lastcommand = datetime.datetime.now()
    if difference.seconds < cooldown_seconds:
        await message.channel.send("`.facade` is on a " + str(cooldown_seconds) + " second cooldown.")
        return
    with open(FILEPATH) as json_file:
        all_facades = json.load(json_file)

    if message.content == ".facade size":
        size = len(all_facades)
        await message.channel.send(str(size) + " total facades available")
        return
    elif message.content == ".facade list":
        facade_names = []
        current_index = 0
        for f in all_facades:
            facade_names.append(str(current_index) + " - " + f["name"] + ": " + f["builder"])
            current_index += 1
        facade_strs = [""]
        for fn in facade_names:
            if len(facade_strs[-1] + fn) > 1990:
                facade_strs.append("")
            facade_strs[-1] = facade_strs[-1] + "\n" + fn

        for mes in facade_strs:
            await message.author.send("```" + mes + "```")

        return


    if message.content == ".facade all" and listadmins.is_user_admin(message):
        await message.channel.send("The bot is about to dm you all " + str(len(all_facades)) + " facades. Are you sure about this? If so, type `.facade all " + str(len(all_facades)) + "`.")
        return
    if message.content == ".facade all " + str(len(all_facades)) and listadmins.is_user_admin(message):
        for idnum in range(len(all_facades)):
            organ = all_facades[idnum]
            await dm_get_single_facade(client, message, organ, idnum)
    elif len(message.content.split()) == 2:
        #All users can use the specific facade now.
        organ = all_facades[int(message.content.split()[1])]
        idnum = int(message.content.split()[1])
        await get_single_facade(client, message, organ, idnum)
    else:
        idnum = random.randrange(0, len(all_facades))
        organ = all_facades[idnum]
        await get_single_facade(client, message, organ, idnum)


async def dm_get_single_facade(client, message, organ, idnum):
    church_name = organ["name"]
    builder = organ["builder"]
    year = organ["year"]
    manuals = organ["manuals"]
    tech = organ["tech"]
    size = ""
    if manuals > 0:
        size = "**Size:** " + get_roman_numeral(manuals) 
        if organ["pedal"]: size += "/P"
        #size +=  ", " + str(ranks) + " ranks"
        size += "\n"
    size += tech
    stoplist = organ["stoplist"]
    img = organ["img"]
    statement = ""
    statement += "\n**Builder:** "
    statement += builder
    statement += "\n**Year:** "
    statement += str(year)
    statement += "\n"
    statement += size
    '''
    statement += "\n**Stoplist:**\n"
    statement += str(stoplist)
    '''

    footer = "id " + str(idnum) + " / Click here to submit correction."
    image = discord.Embed(title=church_name, url=stoplist)
    image.add_field(inline=True,
            name="Description",
            value=statement)
    image.add_field(inline=True,
            name="ID " + str(idnum),
            value="[Correction](" + get_correction_link(idnum, organ) + ")")
    image.set_image(url=img)
        #HERE, either add the image URL or figure out
        #how to attach the image.

    await message.author.send(embed=image)
    return
async def get_single_facade(client, message, organ, idnum):
    church_name = organ["name"]
    builder = organ["builder"]
    year = organ["year"]
    manuals = organ["manuals"]
    tech = organ["tech"]
    size = ""
    if manuals > 0:
        size = "**Size:** " + get_roman_numeral(manuals) 
        if organ["pedal"]: size += "/P"
        #size +=  ", " + str(ranks) + " ranks"
        size += "\n"
    size += tech
    stoplist = organ["stoplist"]
    img = organ["img"]
    statement = ""
    statement += "\n**Builder:** "
    statement += builder
    statement += "\n**Year:** "
    statement += str(year)
    statement += "\n"
    statement += size
    '''
    statement += "\n**Stoplist:**\n"
    statement += str(stoplist)
    '''

    footer = "id " + str(idnum) + " / Click here to submit correction."
    image = discord.Embed(title=church_name, url=stoplist)
    image.add_field(inline=True,
            name="Description",
            value=statement)
    image.add_field(inline=True,
            name="ID " + str(idnum),
            value="[Correction](" + get_correction_link(idnum, organ) + ")")
    image.set_image(url=img)
        #HERE, either add the image URL or figure out
        #how to attach the image.

    await message.channel.send(embed=image)
    return

