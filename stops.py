import discord
import asyncio
import requests

from html.parser import HTMLParser

FULL = "http://www.organstops.org/FullIndex.html"

isOrganStop = False
currentStopUrl = ""

all_stops = dict()

all_letters = "qwertyuiopasdfghjklzxcvbnm"

simple_sub = {
        "ä": "a",
        "ë": "e",
        "ï": "i",
        "ö": "o",
        "ü": "u",
        "û": "u",
        "é": "e",
        "à": "a"

        }

def get_simple(text):
    newtext = ""
    for t in text:
        if t in simple_sub:
            newtext += simple_sub[t]
        else:
            newtext += t
    return newtext

imageURL = None

class fullparser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        global isOrganStop
        global currentStopUrl
        if tag == "a":
            for attr in attrs:
                if attr[0] == "href" and attr[1][0] in all_letters:
                    isOrganStop = True
                    currentStopUrl = attr[1]
    def handle_endtag(self, tag):
        global isOrganStop
        if tag == "a":
            isOrganStop = False

    def handle_data(self, data):
        global isOrganStop
        global currentStopUrl
        global all_stops

        if not isOrganStop: return

        all_stops[data.lower()] = currentStopUrl
        all_stops[get_simple(data.lower())] = currentStopUrl

isParagraph = False
text = ""
shouldIgnore = False
#ignorePhrases = ["this entry is still under", "this page is still under", "revision history", "to see what's", "copyright"]
ignorePhrases = ["copyright"]
class Stopparser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        global imageURL
        global isParagraph
        global text
        if tag == "p":
            isParagraph = True
        if tag == "img":
            isSponsor = False
            tempImage = None
            for attr in attrs:
                if attr[0] == "class" and attr[1] == "Sponsor":
                    isSponsor = True
                if attr[0] == "src":
                    tempImage = attr[1]

            if not isSponsor and imageURL == None and tempImage:
                print(tempImage)
                imageURL = tempImage
        elif tag == "br":
            text += "\n"

    def handle_endtag(self, tag):
        global isParagraph
        if tag == "p":
            isParagraph = False

    def handle_data(self, data):
        global text
        global shouldIgnore
        if data in ["Examples", "Variants", "Bibliography"]:
            shouldIgnore = True

        if isParagraph and not shouldIgnore:
            for p in ignorePhrases:
                if data.lower().startswith(p):
                    return
            text += data



r = requests.get(FULL, allow_redirects= True)
parser = fullparser()
parser.feed(r.text)

plzstop = '.organstop'
async def get_stop(client, message):
    global imageURL
    imageURL = None

    if len(message.content.split()) == 1:
        await message.channel.send("`.organstop [name]`")
        return


    global text
    global shouldIgnore
    stopname = message.content[len(plzstop)+1:].lower().strip()

    if stopname not in all_stops:
        await message.channel.send("I couldn't find that stop listed. You may look for the stopname at " + FULL)
        return

    url = "http://www.organstops.org/" + all_stops[stopname]
    r = requests.get(url, allow_redirects=True)

    imageurlstart = "/".join(url.split("/")[:-1]) + "/"

    text = ""
    shouldIgnore = False
    parser = Stopparser()
    parser.feed(r.text)

    lines = text.split('\n')
    newtext = ''
    for line in lines:
        if line == "": continue
        newtext += line.strip()
        newtext += "\n"

    if len(newtext) > 1900:
        newtext = newtext[:1900] + "\n. . ."
    embed = discord.Embed(title=stopname.title(), url=url, description=newtext)
    if imageURL:
        fullImageURL = imageurlstart + imageURL
        print(fullImageURL)
        embed.set_image(url=imageurlstart + imageURL)

    await message.channel.send(embed=embed)
    
