# pedals.py
# implements .pedal command

import discord
import asyncio

description = '''Please note that you should not use an 8vb Bass Clef in the pedals when writing organ music. This is a bad habit caused by the default selection of clefs by Musescore.

Please refer to [this comment](https://musescore.org/en/comment/1105298#comment-1105298) [(archive)](https://archive.is/hED3H) for specifications and examples in literature. Also refer to [this video](https://youtu.be/fuTr6hOq2tQ) for a demonstration of different pitches in the pedals and manuals. In addition, please refer to all published organ scores from around the last 200 years, during which the three-stave layout was the standard.

Writing an octave clef in the bass leaves organists to form the bad habit of silently ignoring these symbols used to specify octave changes, which will lead to ambiguity when the composer uses these symbols later in the score.

This is also a poor influence, as new composers will be misled about how pipe organ sounding and written pitch interact, especially when the pedals are concerned.

In the Musescore engraving software, it is trivially easy to change the clef of the pedals stave to a bass clef. In addition, the "Organ" instrument can be used instead of "Pipe Organ".'''

async def do_pedalcommand(client, message):
    emb = discord.Embed(title="Musescore Pedal Clef", description=description)
    emb.set_image(url="attachment://pedals.jpg")
    await message.channel.send(file=discord.File("safe/pedals.jpg"), embed=emb)


