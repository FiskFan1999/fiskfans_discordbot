
        #"ab": ["🅰️", "🅱️"],
POLLEMOTES = {
        "ab": ["🅰️", "🅱️"],
        "thumb" : ["👍", "👎"]
        }

restofpoll_letters = "abcdefghijklmnopqrstuvwxyz"
restofpoll_emojis = ["🇦", "🇧", "🇨", "🇩", "🇪", "🇫", "🇬", "🇭", "🇮", "🇯", "🇰", "🇱", "🇲", "🇳", "🇴", "🇵", "🇶", "🇷", "🇸", "🇹", "🇺", "🇻", "🇼", "🇽", "🇾", "🇿"]

for i in range(3, 21):
    POLLEMOTES[restofpoll_letters[:i]] = restofpoll_emojis[:i]
