#Hauptwerk forum 
#all messages link http://forum.hauptwerk.com/search.php?search_id=active_topics

import requests
from html.parser import HTMLParser
import json
import threading

HWFORUMPATH = "./examplesafe/hwforumchannel.json"

def getChannelIDS():
    with open(HWFORUMPATH) as json_file:
        file_json = json.load(json_file)
    return file_json

def getPostsText():
    url = "http://forum.hauptwerk.com/search.php?search_id=active_topics"
    req = requests.get(url)
    status = req.status_code
    print(url, "status", status)
    if status != 200:
        return None
    return req.text

def getPostsDict(htmltext):
    parser = HWPostsParser()
    parser.feed(htmltext)
    return parser.found_posts


class HWPostsParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.found_posts = []
        self.posts_lookForPostName = False
        self.posts_lookForPostAuthor = False

        self.postName = None
        self.postAuthor = None
        self.postHref = None
    def handle_starttag(self, tag, attrs):
        # <td class="topic">
        if tag != "a": return
        href = None
        for att in attrs:
            if att[0] == "class" and att[1] == "topictitle":
                self.posts_lookForPostName = True
            elif att[0] == "href" and att[1].startswith("./memberlist.php?mode="):
                self.posts_lookForPostAuthor = True
            elif att[0] == "href":
                href = att[1]
        if self.posts_lookForPostName:
            self.postHref = href

    def handle_endtag(self, tag):
        if tag != "td": return
        if self.postName and self.postAuthor and self.postHref:
            self.found_posts.append({
                "title": self.postName,
                "author": self.postAuthor,
                "href": self.postHref
                })
            self.postName = None
            self.postAuthor = None
            self.postHref = None

    def handle_data(self, data):
        if self.posts_lookForPostName:
            self.postName = data.strip()
            self.posts_lookForPostName = False
        elif self.posts_lookForPostAuthor:
            self.postAuthor = data.strip()
            self.posts_lookForPostAuthor = False 

lastNewPostTitle = None

def getNewPosts(posts, lastTitle):
    numberOfNewPosts = 0
    for p in posts:
        if p["title"] == lastTitle:
            break
        numberOfNewPosts += 1

    return posts[:numberOfNewPosts]

def hwForumRunLoop():
    while True:
        new_thread = threading.Thread(target=hwForumLoop,name="getMoreId",args=[])
        new_thread.start()

def hwForumLoop(client):
    text = getPostsText()
    posts = getPostsDict(text)
    newPosts = getNewPosts(posts, lastNewPostTitle)
    for newpost in newPosts:
        pass


if __name__ == "__main__":
    text = getPostsText()
    posts = getPostsDict(text)
    lastNewPostTitle = posts[2]["title"]

    print(getNewPosts(posts, lastNewPostTitle))

    print(getChannelIDS())
