#key = server ID, 
#value = channel ID to post images in
#both ints

import discord
import asyncio

imagemod = {
        #Example: #Pipe organ discord server
        #652160276644364288: <channelID>,
        }

async def do_imagemod(client, message):
    return
    for attach in message.attachments:

        attach_filename = attach.filename

        attach_file = await attach.to_file()
        #await message.channel.send(file=attach_file)

        channel = client.get_channel(imagemod[message.guild.id])
        user = message.author.name
        user_id = message.author.id
        #url = attach.proxy_url
        url = "attachment://" + attach_filename
        chan = message.channel.name
        string = "Image sent by <@!"
        string += str(user_id)
        string += "> "
        string += "in <#"
        string += str(message.channel.id)
        string += ">\n(Click \"Open Original\" To view in browser if it can not be viewed here.)"
        emb = discord.Embed(description=string)
        #emb.set_image(url=url)
        emb.set_footer(text=attach_filename)
        
        await channel.send(embed=emb, file=attach_file)
