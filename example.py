import discord
from dotenv import load_dotenv
import asyncio
import json
from io import StringIO
import datetime
import subprocess

from listadmins import is_user_admin

PATH="./examplesafe/example.json"
TITLESPATH = "./examplesafe/titles.json"

admins = [#example: 1234543212345, #user
        ]


DEBUGCHANNEL = {
        652160276644364288: 0#channel id
        }

def get_json(path):
    with open(path) as json_file:
        file_json = json.load(json_file)
    return file_json

def write_json(json_stuff, path):
    with open(path, "w") as json_file:
        json.dump(json_stuff, json_file)
    return

def get_user_history(userID, path):
    if not userID in path:
        path[userID] = dict()
        path[userID]["links"] = []
        path[userID]["sets"] = []
        path[userID]["images"] = []
    return path[userID]


def get_server_examples(client, message):
    json_stuff = get_json(PATH)
    serverid = str(message.guild.id)
    if not serverid  in json_stuff:
        json_stuff[serverid] = dict()

    return  (json_stuff, json_stuff[serverid])

def get_new_id(serverexamples):
    i = 1
    keys = serverexamples.keys()
    while True:
        for key in keys:
            if serverexamples[key]["id"] == i:
                i += 1
                continue
        break
    return i

def get_key_from_id(examples, id_number):

    for key in examples:
        if examples[key]["id"] == id_number:
            return key

def is_id_or_name(name_or_id, examples):
        key = ""
        if len(name_or_id) == 1 and name_or_id[0].isdigit():
            #IS ID NUMBER

            key = get_key_from_id(examples, int(name_or_id[0]))

        else:
            key = ' '.join(name_or_id)
            print(key)
        return key.lower()

def get_youtube_title(url, namesdict):
    # namesdict should be entire dictionary. 
    # Not just the "yt" dictionary.
    if url in namesdict["yt"]:
        return namesdict["yt"][url]

    #else, get the youtube title, put it in the dictionary (will be saved after)
    titleoutput = None
    outputconv = None
    try:
        titleoutput = subprocess.check_output(["youtube-dl", "-e", url])
        outputconv = titleoutput.decode('utf-8').strip()
    except subprocess.CalledProcessError:
        outputconv = url
    print(outputconv)
    namesdict["yt"][url] = outputconv
    return str(outputconv)


        
prompt = ".example"
async def run_command(client, message):
    mes = message.content
    serverid = str(message.guild.id)
    userid = str(message.author.id)
    all_json, examples = get_server_examples(client,message)
    all_user_historytemp = all_json["userhistory"]

    titles_json = get_json(TITLESPATH)

    user_history = get_user_history(userid, all_user_historytemp)



    banned_list = all_json["banned"]
    if str(message.author.id) in banned_list:
        return

    if mes == prompt:
        await message.channel.send("`.example tutorial`")
        return

    if mes == prompt + " tutorial":
        await message.channel.send("*Will send you a direct-message. Please check that your settings allow server members to send you direct-messages.*")

        f = open("safe/example_tutorial", "r")
        tutorialtext = f.read()
        f.close()
        emb = discord.Embed(title=".example", description=tutorialtext)
        await message.author.send(embed=emb)


        return

    elif mes.startswith(prompt + " admin" ) and await is_user_admin(message):
        f = open("safe/example_admin_tutorial", "r")
        tutorialtext = f.read()
        f.close()
        emb = discord.Embed(title=".example", description=tutorialtext)
        await message.author.send(embed=emb)
        return

    parts = mes.split(" ")
    if mes == prompt + " list":
        #LIST ALL EXAMPLE SETS ALREADY MADE
        ''

        directory = []
        index_list = {}

        for key in examples.keys():
            index_list[int(examples[key]["id"])] = str(key)

        index_keys = list(index_list.keys())
        index_keys.sort()

        for key in index_keys:
            #directory.append(str(index_list[key]) + "\t - " + str(key))
            directory.append(str(key) + " \t- " + str(index_list[key]))

        pages = []
        pageindex = 0
        pages.append("")
        for l in directory:
            if len(pages[-1]) + len(l) > 1997:
                pages.append("")
            pages[-1] = pages[-1] + l + "\n"

        pageNumber=0
        for page in pages:
            pageNumber += 1
            pagetitle = message.guild.name + " Examples - Page " + str(pageNumber)
            emb = discord.Embed(title=pagetitle, description=page)
            await message.author.send(embed = emb)



        
    elif mes.startswith( prompt + " add"):
        #add a new entry into an ALREADY EXISTING
        #set.
        '.example add <url> <name/id>'
        url = parts[2]

        name_or_id = parts[3:]
        key = is_id_or_name(name_or_id, examples)
        '''
        if len(name_or_id) == 1 and name_or_id[0].isdigit():
            #IS ID NUMBER

    /d        key = get_key_from_id(examples, int(name_or_id[0]))

        else:
            key = ' '.join(name_or_id)
            print(key)
        '''

        if key in examples and not url in examples[key]["links"]:
            examples[key]["links"].append(url)
            get_youtube_title(url, titles_json)
            if not url in user_history["links"]: user_history["links"].append(url)
            await message.channel.send("Added link to " + key)
        elif key in examples and url in examples[key]["links"]:
            await message.channel.send("That link has already been added to this example set.")
        else:
            await message.channel.send("That does not appear to be a correct key. Perhaps try `.example list` and enter it by id?")




    elif mes.startswith( prompt + " setimage"):
        #add a new entry into an ALREADY EXISTING
        #set.
        '.example setimage <url> <name/id>'
        url = parts[2]
        name_or_id = parts[3:]
        key = is_id_or_name(name_or_id, examples)
        if not key in examples:
            await message.channel.send("It appears you didn't enter in an appropriate example set name or id.")
        else:
            print(url)
            examples[key]["image"] = url
            await message.channel.send("Image set.")
            if not url in user_history["images"]: user_history["images"].append(url)


    elif mes.startswith( prompt + " new"):
        #creating a NEW SET
        '.example new <name>'

        new_entry = dict()
        new_entry["id"] = get_new_id(examples)

        name_parts = mes.lower().split(" ")[2:]
        name = ' '.join(name_parts)
        print("name")
        key = name

        links = list()
        new_entry["links"] = links
        image = None
        new_entry["image"] = image

        if not key in examples:
            examples[key] = new_entry
            if not name in user_history["sets"]: user_history["sets"].append(name)
            await message.channel.send("New set: " + key)
        else:
            await message.channel.send("It appears that is already a available example set in this server.")


    elif mes.startswith(prompt + " clearimage" ) and await is_user_admin(message):
        setidsraw = parts[2:]
        setkeys = []
        for i in setidsraw:
            setkeys.append(is_id_or_name(i, examples))

        for key in setkeys:
            examples[key]["image"] = None
            await message.channel.send("Cleared image: " + key)

        
    elif mes.startswith(prompt + " merge" ) and await is_user_admin(message):
        merge_to = parts[2]
        will_delete = parts[3:]

        merge_tokey = is_id_or_name(merge_to, examples)
        will_deletekeys = []
        for w in will_delete:
            will_deletekeys.append(is_id_or_name(w, examples))

        merged_links = []
        for i in will_deletekeys:
            merged_links += (examples[i]["links"])

        examples[merge_tokey]["links"] += merged_links
        await message.channel.send("Sets merged.")

    elif mes.startswith(prompt + " remove " ) and await is_user_admin(message):
        url = parts[2]

        sets = list(examples.keys())
        if len(parts) > 3:
            ids = parts[3:]
            sets = []
            for i in ids:
                sets.append(is_id_or_name(i, examples))


        for key in sets:
                if url in examples[key]["links"]:
                    examples[key]["links"].remove(url)
                    await message.channel.send("Removed link from "+ key)
        for user in all_json["userhistory"]:
            if url in all_json["userhistory"][user]["links"]:
                #all_json["userhistory"][user]["links"].remove(url)
                pass




    elif mes.startswith(prompt + " delete" ) and await is_user_admin(message):

        name_or_id= parts[2:]
        key = is_id_or_name(name_or_id, examples)

        if not key in examples:
            await message.channel.send("Not a example set.")
        else:
            examples.pop(key)
            for user in all_json["userhistory"]:
                print(all_json["userhistory"][user])
                if key in all_json["userhistory"][user]["sets"]:
                    pass
                    #all_json["userhistory"][user]["sets"].remove(key)

            await message.channel.send("Deleted.")

    elif mes.startswith(prompt + " removeallfromuser" ) and await is_user_admin(message):
        users = parts[2:]

        for user in users:
            log = all_json["userhistory"][user]
            links = log["links"]
            sets = log["sets"]
            #images = log["images"]

            setkeys = list(examples.keys())
            for key in setkeys:
                for link in links:
                    if link in examples[key]["links"]:
                        examples[key]["links"].remove(link)

                
                for usersSet in sets:
                    if usersSet == key:
                        examples.pop(key)

            images = log["images"]

            setkeys = list(examples.keys())
            for key in setkeys:
                if examples[key]["image"] in images:
                    examples[key]["image"] = None



        await message.channel.send("Removed from user")


    elif mes.startswith(prompt + " unban" ) and await is_user_admin(message):
        banid = parts[2]
        if banid in banned_list:
            banned_list.remove(banid)
            await message.channel.send("unbanned")
        else:
            await message.channel.send("User id not found")
    elif mes.startswith(prompt + " ban" ) and await is_user_admin(message):
        banid = parts[2]
        banned_list.append(banid)
        await message.channel.send("banned")
    else:
        #ALL THE ARGS AFTER .example are the id or name
        id_or_name = parts[1:]
        key = is_id_or_name(id_or_name, examples)

        if not key in examples:
            await message.channel.send("You did not appear to request a valid example set. Maybe try `.example list` and use the id number?")

        this_example = examples[key]

        title = key.capitalize()
        id_text = "id: " + str(this_example["id"])
        image = this_example["image"]
        links = this_example["links"]

        links_per_page = 30 
        links_text = []
        links_text.append("")

        for i in range(0, len(links)):
            #links_text.append("")
            url = links[i]
            urltitle = get_youtube_title(url, titles_json)
            if urltitle == None:
                urltitle = url

            newline = "[" + urltitle[:1995 - len(links[i])] + "](" + links[i] + ")\n"
            if len(links_text[-1]) + len(newline) > 1999:
                links_text.append("")

            links_text[-1] += newline

        for page in links_text:
            emb = discord.Embed(title=title, description=page[:2000])
            if image != None:
                emb.set_image(url=image)
            await message.channel.send(embed=emb)


    if message.guild.id in DEBUGCHANNEL and parts[1].lower() in ["add", "new", "setimage", "remove", "delete", "ban", "unban"]:
        channel = client.get_channel(DEBUGCHANNEL[message.guild.id])
        s = '`' + str(datetime.datetime.now())
        s += "`\n<@!" + str(message.author.id) + '> / **' + message.guild.name + '** / <#' + str(message.channel.id) + ">"
        s += "\n" + message.content

        await channel.send(embed=discord.Embed(description=s))

        await user.send(embed=discord.Embed(description=s))

    all_user_historytemp[userid] = user_history
    all_json["userhistory"] = all_user_historytemp
    all_json[serverid] = examples
    all_json["banned"] = banned_list
    write_json(all_json, PATH)
    write_json(titles_json, TITLESPATH)


