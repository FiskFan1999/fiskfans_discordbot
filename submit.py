import discord
from dotenv import load_dotenv
import asyncio

async def legal(client, message):
    f = open("safe/legal_message", "r")
    legal_message = f.read()
    f.close()
    emb = discord.Embed(description=legal_message)
    await message.channel.send(embed=emb)

async def submit(client, message):
    f = open("safe/submit_message", "r")
    submit_message = f.read()
    f.close()
    emb = discord.Embed(description=submit_message)
    await message.channel.send(embed=emb)

async def credits(client, message):
    f = open("safe/credits_message", "r")
    credits_message = f.read()
    f.close()
    emb = discord.Embed(description=credits_message)
    await message.channel.send(embed=emb)

