import discord
import asyncio
import json

# NOTES
# mentioning a role: starts with <@& ends with >
# mentioning a user: starts with <@ ends with >

async def admincommand(client, message):
    if message.content.lower()[1:] == "admin list":
        current_admin_list = get_admin_list_for_server(message)
        currentserveradmins = current_admin_list[str(message.guild.id)]
        userstext = ""
        for user in currentserveradmins["users"]:
            userstext += "<@!" + str(user) + ">\n"
        print(userstext)

        rolestext = ""
        for role in currentserveradmins["roles"]:
            rolestext += "<@&" + str(role) + ">\n"

        globaltext= ""
        for role in current_admin_list["force"]:
            globaltext+= "<@!" + str(role) + ">\n"

        embed = discord.Embed(title="Fiskfansbot Admins (" + message.guild.name +")")

        if userstext:
            embed.add_field(inline=True, name="Users", value=userstext)
        if rolestext:
            embed.add_field(inline=True, name="Roles", value=rolestext)
        embed.add_field(inline=False, name="Global", value=globaltext)

        await message.channel.send(embed=embed)


        return


    if not await is_user_admin(message):
        return

    guild_id = str(message.guild.id)

    current_admin_list = get_admin_list_for_server(message)

    parts = message.content.lower().split()
    prompt = parts[1]
    await message.channel.send(prompt)

    if prompt == "adduser":
        user_ids = parts[2:]
        if len(user_ids) == 0:
            await message.channel.send("`.admin adduser <userid> <userid> . . .`")
            return
        
        for i in user_ids:
            if i[0:2] == "<@" and i[2] == "!" and i[-1] == ">":
                idint = int(i[3:-1])
                if idint not in current_admin_list[guild_id]["users"]:
                    current_admin_list[guild_id]["users"].append(int(i[3:-1]))
                else:
                    await message.channel.send("<@!" + str(idint) + "> is already an admin")
            else:
                await message.channel.send("Error: " + i)

    elif prompt == "addrole":
        user_ids = parts[2:]
        if len(user_ids) == 0:
            await message.channel.send("`.admin addrole <roleid> <roleid> . . .`")
            return
        
        for i in user_ids:
            if i[0:2] == "<@" and i[2] == "&" and i[-1] == ">":
                idint = int(i[3:-1])
                if idint not in current_admin_list[guild_id]["roles"]:
                    current_admin_list[guild_id]["roles"].append(idint)
                else:
                    await message.channel.send("Role <@!" + str(idint) + "> is already admins")
            else:
                await message.channel.send("Error: " + i)

    elif prompt == "removeuser":
        user_ids = parts[2:]
        if len(user_ids) == 0:
            await message.channel.send("`.admin removeuser <userid> <userid> . . .`")
            return
        
        for i in user_ids:
            if i[0:2] == "<@" and i[2] == "!" and i[-1] == ">":
                idint = int(i[3:-1])
                if idint in current_admin_list[guild_id]["users"]:
                    current_admin_list[guild_id]["users"].remove(idint)
                else:
                    await message.channel.send("<@!" + str(idint) + "> is not an admin")
            else:
                await message.channel.send("Error: " + i)
    elif prompt == "removerole":
        user_ids = parts[2:]
        if len(user_ids) == 0:
            await message.channel.send("`.admin removerole <roleid> <roleid> . . .`")
            return
        
        for i in user_ids:
            if i[0:2] == "<@" and i[2] == "&" and i[-1] == ">":
                idint = int(i[3:-1])
                if idint in current_admin_list[guild_id]["roles"]:
                    current_admin_list[guild_id]["roles"].remove(idint)
                else:
                    await message.channel.send("Role <@&" + str(idint) + "> not admin")
            else:
                await message.channel.send("Error: " + i)


    write_admin_json(current_admin_list)


    


ADMINLISTPATH = "./dontuploadsafe/admins.json"
def get_admin_json():
    path = ADMINLISTPATH
    with open(path) as json_file:
        file_json = json.load(json_file)
    return file_json

def write_admin_json(json_stuff):
    path = ADMINLISTPATH
    with open(path, "w") as json_file:
        json.dump(json_stuff, json_file)
    return

def get_admin_list_for_server(message):
    #pass message object to get guild id

    guild_id = str(message.guild.id)
    admin_list_whole = get_admin_json()


    if guild_id not in admin_list_whole:
        admin_list_whole[guild_id] = {"users": [], "roles": []}
        write_admin_json(admin_list_whole)

    return admin_list_whole

async def is_user_admin(message):
    full_list = get_admin_list_for_server(message)
    current_admin_list, forced_list = full_list[str(message.guild.id)], full_list["force"]
    print(current_admin_list, forced_list)

    guild = message.guild
    user_id = message.author.id

    if user_id in forced_list or user_id in current_admin_list["users"]:
        return True


    #look by role.

    print("guild obj", guild)
    memberobj = await guild.fetch_member(user_id)
    print("member obj", memberobj)

    user_roles = memberobj.roles

    for role in user_roles:
        i = role.id
        if i in current_admin_list["roles"]:
            return True

    return False

