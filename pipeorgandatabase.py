import discord
import asyncio
import requests
import random
import subprocess
import threading

import maketextfile

from html.parser import HTMLParser

indexzipfile = "./safe/ohs/index.xz"
usersalreadysentzip = []

stoplistData = "stoplistData"
nodeEnding = "./pipeorgandatabasenodeending.js"

def isNewStoplistJavascript(data):
    #data is all of the html
    return stoplistData in data

def parseNewStoplistJavascript(javascript):
    #javascript is the text within the <script> block (already cut down)
    #add the snippet in the .js file, parse it through `node` and return the output.
    parser = JavascriptStoplistParser()
    parser.feed(javascript)
    stoplistjs = parser.scriptText

    f = open(nodeEnding)
    endOfNodeFile = f.read()
    f.close()

    f = open("nodeTemp.js", "w")
    f.write(stoplistjs)
    f.write("\n")
    f.write(endOfNodeFile)
    f.close()

    stoplistParsed = subprocess.check_output(["node", "nodeTemp.js"])

    return stoplistParsed.decode()

class JavascriptStoplistParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.isScript = False
        self.scriptText = None
    def handle_starttag(self, tag, attrs):
        if tag == "script":
            self.isScript = True
    def handle_endtag(self, tag):
        if tag == "script":
            self.isScript = False
    def handle_data(self, data):
        if stoplistData in data and self.isScript and self.scriptText == None:
            index = data.index("};")
            self.scriptText = (data[:index + 2])

async def sendIndexZipToUser(message):
    #return TRUE if it was successful
    try: await message.author.send(file=discord.File(indexzipfile))
    except discord.errors.Forbidden:
        await message.channel.send("Message dm failed. Have you allowed the bot to send a direct-message to you?")
        return False

    return True


banned_images = [
        "/static/img/ohs-logo-w-color.jpg",
        "/img/sponsors/"
        ]
images = []
class ImageRequestParser(HTMLParser):

    def handle_starttag(self, tag, attrs):
        global images
        if tag == "img":
            for att in attrs:
                if att[0] == "src":
                    if not att[1] in banned_images:
                        for text in banned_images:
                            if text in att[1]:
                                return
                        images.append(att[1])

is_stoplist = False
stopliststr = ""
class myhtmlparser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        global stopliststr
        global is_stoplist
        if tag == "pre":
            is_stoplist = True


    def handle_endtag(self, tag):
        global stopliststr
        global is_stoplist
        if tag == "pre":
            is_stoplist = False

    def handle_data(self, data):
        global stopliststr
        if is_stoplist:
            stopliststr += data

image_ids = []
def getRandomID():
    if len(image_ids) > 0:
        return image_ids.pop(0)
    return getRandomIDSlow()

def getRandomIDSlow():
    return 0 #bypass this process
    global images
    imageParser = ImageRequestParser()
    while True:
        i = random.randint(1, 70000)
        print("trying to get pod id with image", i)
        url = "https://pipeorgandatabase.org/organ/" + str(i)
        req = requests.get(url)
        text = req.text
        images = []
        imageParser.feed(text)
        if len(images) > 0:
            return i

def putMoreRandomIDSLoop():
    new_thread = threading.Thread(target=putMoreRandomIDS,name="getMoreId",args=[])
    new_thread.start()

def putMoreRandomIDS():
    global image_ids
    for i in range(20):
        i = getRandomIDSlow()
        image_ids.append(i)
        print("image_ids len", len(image_ids))

putMoreRandomIDSLoop()
#nextRandomID = getRandomID()

organNameStr = ""
isOrganName = False
location = ""
isLocation = False
stoplisturl = ""
stopliststr = ""
class myhtmlparserOrgan(HTMLParser):
    def handle_starttag(self, tag, attrs):
        global organNameStr
        global isOrganName
        global location
        global isLocation
        global stoplisturl
        if tag == "title":
            isOrganName = True
        if tag == "p" and ("class", "card-text") in attrs:
            #print("FOUND IT")
            isLocation = True
        elif tag == "a":
            for attr in attrs:
                if attr[0] == "href" and attr[1].startswith("/stoplist/"):
                    print("FOIND STOPLIST")
                    print(attr[1])
                    stoplisturl = attr[1]


    def handle_endtag(self, tag):
        global organNameStr
        global isOrganName
        global isLocation
        if tag == "title":
            isOrganName = False 
        elif tag == "p":
            isLocation = False

    def handle_data(self, data):
        global organNameStr
        global location
        if isOrganName:
            organNameStr += data
        elif isLocation:
            dataparts = data.split("\n")
            for newdata in dataparts:
                if newdata.strip() == "": continue
                while len(newdata) > 0 and newdata[0]== " ":
                    newdata = newdata[1:]

                location += newdata + "\n"

isTechnicalDetails = False
technicalDetails = ""

class organDataParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        pass



    def handle_endtag(self, tag):
        global isTechnicalDetails
        if tag == "div":
            isTechnicalDetails = False

    def handle_data(self, data):
        global isTechnicalDetails
        global technicalDetails
        if isTechnicalDetails and data.strip() != "":
            for line in data.split("\n"):
                if line.strip() != "": 
                    technicalDetails += line.strip().strip(chr(32))
                    if technicalDetails.strip()[-1] == ":": 
                        technicalDetails += " "
                    else:
                        technicalDetails += "\n"
        if data == "Technical Details:":
            isTechnicalDetails = True

isNotes = False
isWritingNotes = False
organNotes = []

class notesParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        global isNotes
        global isWritingNotes
        global organNotes
        if tag == "p" and isNotes:
            isWritingNotes = True
            organNotes.append("")

        for att in attrs:
            if isWritingNotes or isNotes:
                print(att)
            if att == ("role", "tablist"):
                isWritingNotes = False
                isNotes = False




    def handle_endtag(self, tag):
        global isNotes
        global isWritingNotes
        global organNotes
        if tag == "p":
            isWritingNotes = False

    def handle_data(self, data):
        global isNotes
        global isWritingNotes
        global organNotes
        if data == "Notes":
            isNotes = True
            return
        if data == "Instrument Images:":
            isNotes = False
            isWritingNotes = False

        if isWritingNotes and isNotes:
            organNotes[-1] += data


#jparser.feed('<html><head><title>Test</title></head>'
 #           '<body><h1>Parse me!</h1></body></html>')


BEGINNING = "<pre style=\"font-family: 'Oxygen Mono', monospace;font-weight:400;\">"
END= "</pre>"

prompt = '.pod'
async def pod(client, message):
    global stopliststr
    global is_stoplist
    arg = message.content.lower().split()
    if len(arg) == 1 :
        await message.channel.send("`.pod [organ/stoplist] [id]`")
        return


    #await message.channel.send("Fetching information from the Pipe Organ Database:")
    
    kind = arg[1]
    if len(arg) >= 3: 
        pod_id = arg[2]
    else:
        pod_id = 0

    suf = "organ"
    if kind == "index":
        #send index zip file to user.
        userid = message.author.id
        if userid in usersalreadysentzip:
            await message.channel.send("Already sent the index zip file to you!")
            return
        if await sendIndexZipToUser(message):
            usersalreadysentzip.append(userid)
        return
    elif kind == "search" or kind == "query" or kind == "q":
        cmd = ["./searchPOD" ] + arg[2:]
        async with message.channel.typing():
            result = subprocess.run(cmd, stdout=subprocess.PIPE)
            stdout = result.stdout.decode("utf-8").replace("\n", "\n\n")
            
        if len(stdout) > 1900 :
            await message.channel.send("Too many results.")
            return
        elif len(stdout) == 0:
            emb = discord.Embed(title="No results found.")
            await message.channel.send(embed=emb)
            return
        emb = discord.Embed(title="Search results", description= "```" + stdout + "```")
        await message.channel.send(embed=emb)
        return


    elif kind == "organ" or kind == "o" or kind == "surprise" or kind == "r":
        if kind in ["surprise", "r"]:
            await message.channel.send("Random pipe organ database facades is temporarily disabled.")
            return

        async with message.channel.typing():
            if kind in ["surprise", "r"]:

                await message.channel.send("Random pipe organ database facades is temporarily disabled.")
                return
                pod_id = str(getRandomID())
            old_url = "./ohs/" + "organs" + "/" + str(pod_id) + ".html"
            url = "https://pipeorgandatabase.org/organ/" + str(pod_id)
            req = requests.get(url)
        text = req.text

        
        #try:
            #organ = open(url, "r")
            #text = organ.read()
            #organ.close()
        #except FileNotFoundError:
            #await message.channel.send("File not found")
            #return


        global organNameStr
        global location
        global stoplisturl
        global is_stoplist
        global isTechnicalDetails
        global technicalDetails
        global isNotes
        global isWritingNotes
        global organNotes
        global images
        
        is_stoplist = False
        stopliststr = ""
        organNameStr = ""
        location = ""
        isOrganName = isLocation = False
        stoplisturl = ""

        isTechnicalDetails = False
        technicalDetails = "\n"

        isNotes = isWritingNotes = False
        organNotes = []

        parser = myhtmlparserOrgan()
        parser.feed(text)

        parser2 = organDataParser()
        parser2.feed(text)

        parser3 = notesParser()
        parser3.feed(text)
        print(organNotes)

        images = []
        parser4 = ImageRequestParser()
        parser4.feed(text)
        print(images)

        if organNameStr.startswith("Pipe Organ Database |"):
            organNameStr = organNameStr[len("Pipe Organ Database |"):]

        if technicalDetails[-1] == "\n": technicalDetails = technicalDetails[:-1]
        location += technicalDetails

        '''
        if stoplisturl: location += ("\n\nstoplist: https://pipeorgandatabase.org" + stoplisturl
                + "\n.pod s " + stoplisturl.split('/')[-1] )
        '''

        for note in organNotes:
            if len(location) + len(note) > 1900:
                print("LOCATION TOO LONG BREAKING NOW")
                break
            location += "\n\n" + note
            print("length now", len(location))

        #try to get image

        imageFound = False
        filename = "ohs/images/" + str(pod_id) + "/0.jpg"
        #try:
            #imageFile = open(filename, "rb")
#
            #f = discord.File(imageFile, filename="image.jpg")
            #imageFile.close()
            #imageFound = True
        #except FileNotFoundError:
            #print("Image file not found.")

        basicUrl = "https://pipeorgandatabase.org/organ/" + str(pod_id)
        basicImageUrl = None
        if len(images) > 0:
            if images[0][0] == "/":
                basicImageUrl = "https://pipeorgandatabase.org" + images[0]
            else:
                basicImageUrl = images[0]
        emb = discord.Embed(url=basicUrl, title=organNameStr,  description="```" + location[:2041] + "```")
        print("DESCRIPTINO LENGTH", len(location))
        emb.set_footer(text="Organ ID: " + str(pod_id), icon_url="https://pipeorgandatabase.org/static/img/ohs-logo-w-color.jpg")
        if basicImageUrl:
            emb.set_image(url=basicImageUrl)
        await message.channel.send(embed=emb)

        # if was random, get the next id.
        if kind in ["surprise", "r"]:
            nextRandomID = getRandomID()


        #await message.channel.send("**" + organNameStr + "**")
        #await message.channel.send(location)
        
        #if stoplisturl: await message.channel.send("stoplist: https://pipeorgandatabase.org" + stoplisturl)
    elif kind == "stoplist" or kind == "s":
        path = "ohs/stoplists/" + str(pod_id) + "/0.html"

        stoplisturl = ""
        getStoplistHref = requests.get("https://pipeorgandatabase.org/organ/" + str(pod_id))
        getStoplistHrefText = getStoplistHref.text
        getStoplistParser = myhtmlparserOrgan()
        getStoplistParser.feed(getStoplistHrefText)
        if stoplisturl == "":
            await message.channel.send("Not found?")
            return
        stoplisturl_full = "https://pipeorgandatabase.org" + stoplisturl
        stoplistreq = requests.get(stoplisturl_full)
        raw = stoplistreq.content.decode()
        stopliststr = ""
        if (isNewStoplistJavascript(raw)):
            javascriptoutput = parseNewStoplistJavascript(raw)
            stopliststr = javascriptoutput
            #await message.channel.send(javascriptoutput[:2000])
        else:
            content = stoplistreq.text
            parser = myhtmlparser()
            parser.feed(content)
        if len(stopliststr) > 1900:
            #emb = discord.Embed(url=stoplisturl_full, title="The specification is too large to post. (More than 2000 Characters)")
            f = maketextfile.maketextfile(stopliststr)
            await message.channel.send(file=f)

        elif not 1:
            emb = discord.Embed( title="Organ specification is not html. Click here to view.", url=url)
            await message.channel.send(embed=emb)
        else:
            emb = discord.Embed(url=stoplisturl_full, title="Organ Specification", description="```nim\n" + stopliststr + "```")
            await message.channel.send(embed=emb)


    #TO DO: PARSE WEBPAGE HTML HERE
    #await message.channel.send(url)

    '''
    r = requests.get(url, allow_redirects=True)
    is_html = r.headers.get("content-type").startswith("text/html")
    content = r.text
    '''




    #await message.channel.send(url)

    #add more ids
    if len(image_ids) < 10:
        putMoreRandomIDSLoop()
    return
    
