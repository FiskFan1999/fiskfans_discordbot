#commands that can get information from an organbuilders website.

import asyncio
import discord
import requests
import unidecode
from html.parser import HTMLParser
import os
import sys

rosalesBanned = ["", "home"]
rosalesbannedimages = {"https://rosales.com/wp-content/uploads/2016/08/Rosales-Arrrrr-faded.png", "https://rosales.com/wp-content/uploads/2016/08/logo-nav.png"}
rosalesBannedStoplist = {
        "Team",
        "Contact",
        "Instruments",
        "Media",
        "Testimonials",
        "Team\nContact\nInstruments\nMedia\nTestimonials"
        }
rosalesPrompt = ".rosales"

class RosalesOrganInformation:
    def __init__(self):
        self.title = None
        self.paragraphs = []
        self.stoplist = []
        self.imagehref = None

class RosalesOrganPageParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.organinformation = RosalesOrganInformation()
        self.isParagraph = False
        self.isTitle = False
        self.stoplistKillswitch = False
        self.isStoplist = False

    def handle_starttag(self, tag, attrs):
        if tag == "p": self.isParagraph = True
        if tag == "title": self.isTitle = True
        if tag == "img":
            for a in attrs:
                if a[0] == "src" and not a[1] in rosalesbannedimages and self.organinformation.imagehref == None:
                    self.organinformation.imagehref = a[1]
        if tag == "li" and not self.stoplistKillswitch:
            for a in attrs:
                if "menu" in a[1]:
                    return
            self.isStoplist = True
    def handle_endtag(self, tag):
        if tag == "p": self.isParagraph = False
    def handle_data(self, data):
        st = data.strip()

        if self.isStoplist:
            if len(self.organinformation.stoplist) and st == self.organinformation.stoplist[0]:
                self.stoplistKillswitch = True
            else:
                self.organinformation.stoplist.append(st)
            self.isStoplist = False

        if self.isTitle and self.organinformation.title == None:
            self.organinformation.title = st
            self.isTitle = False
        if not st: return
        if self.isParagraph and len(data) > 149 and not st in self.organinformation.paragraphs:
            self.organinformation.paragraphs.append(st)
            #print(st[:40])

class RosalesOpusListParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.opuslinks = dict()
    def handle_starttag(self, tag, attrs):
        if tag != "a": return
        for a in attrs:
            if a[0] == "href":
                hrefsplit = a[1].split("/")
                #['https:', '', 'rosales.com', 'opus-30', '']
                if len(hrefsplit) >= 4 and hrefsplit[0] == "https:" and hrefsplit[1] == "" and hrefsplit[2] == "rosales.com" and not hrefsplit[3] in rosalesBanned:
                    opus = hrefsplit[3]
                    self.opuslinks[opus] = a[1]
    def handle_endtag(self, tag):
        pass
    def handle_data(self, data):
        pass

rosalesInstrumentWebsite = requests.get("https://rosales.com/home/instruments/")

rosalesOpus = RosalesOpusListParser()
rosalesOpus.feed(rosalesInstrumentWebsite.content.decode())

def getRosalesHelp():
    keys = list(rosalesOpus.opuslinks.keys())
    keys.sort()
    firstline = ".rosales "
    nextline  = "         "
    description = ""

    i = 0
    for k in keys:
        description += "\n"
        if i == 0:
            description += firstline
            i = 1
        else:
            description += nextline
        
        description += k

    emb = discord.Embed(description="```" + description + "```")
    return emb

def rosalesGetOrganInformation(url):
    r = requests.get(url)
    content = r.content.decode()
    parse = RosalesOrganPageParser()
    parse.feed(content)
    return parse.organinformation

async def rosalesCommand(client, message):
    argv = message.content.split()
    if len(argv) == 1:
        await message.channel.send(embed=getRosalesHelp())
        return

    opus = argv[1].lower()

    if len(argv) in {2, 3} and opus in rosalesOpus.opuslinks.keys():
        pass
    else:
        await message.channel.send(embed=getRosalesHelp())
        return

    isStoplist = False
    if len(argv) == 3 and argv[2].lower() in {"stoplist", "s"}:
        isStoplist = True

    url = rosalesOpus.opuslinks[opus]
    information = rosalesGetOrganInformation(url)
    description = ""
    if isStoplist:
        description = "\n".join(information.stoplist)[:2000]
    else:
        description = "\n\n".join(information.paragraphs)[:2000]
    emb = discord.Embed(title=information.title, url=url, description=description)
    imageFile = None
    if information.imagehref != None:
        imageFile = getRichardsFowkesImageFile(information.imagehref)
        emb.set_image(url="attachment://organbuilderimage.jpg")
    await message.channel.send(embed=emb, file=imageFile)
    os.remove("./organbuilderimage.jpg")


def richardsFowkesGetOpusUrl(opusint):
    base = "http://www.richardsfowkes.com/3_organs/"
    o = str(opusint).zfill(2)
    return base + o + "/"

richardsFowkesPrompt = ".rf"

async def richardsFowkes(client, message):
    argv = message.content.split()[1:]
    if (not argv):
        await message.channel.send(richardsFowkesHelpMessage)
        return
    opusNumberStr = argv[0]

    try: int(opusNumberStr)
    except ValueError:
        await message.channel.send(richardsFowkesHelpMessage)
        return


    url = richardsFowkesGetOpusUrl(opusNumberStr)
    r = requests.get(url)
    if r.status_code != 200:
        #Not found
        await message.channel.send("Richards Fowkes opus number not found.")
        return

    rfinfo = getRichardsFowkesInfo(r.content.decode())
    #await message.channel.send(rfinfo.title)
    #await message.channel.send(rfinfo.body)
    #await message.channel.send(rfinfo.imagehref)

    emb = discord.Embed(title=", ".join(rfinfo.title), url=url, description="\n\n".join(rfinfo.body)[:2000])
    sendfile = None
    if rfinfo.imagehref:
        emb.set_image(url="attachment://organbuilderimage.jpg")
        sendfile = getRichardsFowkesImageFile(rfinfo.imagehref)

    await message.channel.send(embed=emb, file=sendfile)

    os.remove("./organbuilderimage.jpg")

richardsFowkesHelpMessage = '.rf <opus number>'

class RichardsFowkesInfo:
    def __init__(self):
        self.title = []
        self.body = [] #List of strings for the paragraph
        self.imagehref = None

class RichardsFowkesParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.rfinfo = RichardsFowkesInfo()
        self.isParagraph = False
        self.isTitle = False

    def handle_starttag(self, tag, attrs):
        if tag == "p": self.isParagraph = True

        if tag == "div":
            for a in attrs:
                if a[0] == "id" and a[1] == "addressBox":
                    self.isTitle = True
        if tag == "a":
            self.isTitle = False

        if tag == "img":
            for a in attrs:
                if a[0] == "src" and a[1].startswith("/media/instruments/") and self.rfinfo.imagehref == None:
                    #print((a[1]))
                    self.rfinfo.imagehref = "http://www.richardsfowkes.com" + a[1]

    def handle_endtag(self, tag):
        if tag == "p": self.isParagraph = False
    def handle_data(self, data):
        if self.isParagraph and data.strip():
            self.rfinfo.body.append(data)
        elif self.isTitle and data.strip():
            self.rfinfo.title.append(data.strip())

def getRichardsFowkesInfo(body):
    parser = RichardsFowkesParser()
    parser.feed(body)
    return parser.rfinfo

def getRichardsFowkesImageFile(imageurl):
    r = requests.get(imageurl)
    imagebytes = r.content
    f = open("organbuilderimage.jpg", "wb")
    f.write(imagebytes)
    f.close()
    return discord.File("organbuilderimage.jpg")
