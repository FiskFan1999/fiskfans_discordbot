import discord
import asyncio


async def playcustomstream(client, message):
    return
    if len(message.content.split()) not in [2, 3]:
        #wrong number of args
        await message.channel.send("Wrong number of args")
        return

    url = message.content.split()[1]

    if len (message.content.split()) == 3:
        try: voice_channel = client.get_channel(int(message.content.split()[2]))
        except TypeError:
            await message.channel.send("arg 3 must be a channel id.")
            return
    else:
        try:
            voice_channel = client.get_channel(message.author.voice.channel.id)
        except AttributeError:
            #User was not in a voice channel
            await message.channel.send("You need to be in a voice channel.")
            return

    try: vc = await voice_channel.connect()
    except AttributeError:
        await message.channel.send("Attribute error (joining vc)")
        return
    src = discord.FFmpegPCMAudio(url, options="-map 0:a:0")
    vc.play(src)

    while vc.is_playing():
        await asyncio.sleep(1)
    await vc.disconnect()

async def playRadio(client, message):
    argc = len(message.content.split())
    if (argc == 1):
        await message.channel.send(help_message)
        return

    url = ""

    station = message.content.split()[1]

    if (station == "electric"):
        url = "https://listen-electricradio.sharp-stream.com/electricradio.aac"
        params = None
    elif (station == "trancebase"):
        url = "https://listen.trancebase.fm/tunein-mp3"
        params = "-f mp3"
    elif (station == "technobase"):
        url = "https://listen.technobase.fm/tunein-mp3"
        params = "-f mp3"
    elif (station == "organlive"):
        url = "http://play.organlive.com:7000/320"
        params = "-f mp3"
    elif (station == "baroque"):
        url = "http://play.organlive.com:7002/128"
        params = "-f mp3"
    elif (station == "experience"):
        url = "http://play.organlive.com:7006/320"
        params = "-f mp3"
    else:
        return

    voice_channel = client.get_channel(int(message.author.voice.channel.id))

    vc = await voice_channel.connect()


    for i in range(10):
        try:
            src = discord.FFmpegPCMAudio(url, before_options=params)
            #await asyncio.sleep(2)
            vc.play(src)
            break
        except:
            print("Error")
            #await message.channel.send(err)

    while (message.author.voice != None and message.author.voice.channel.id == voice_channel.id):
        await asyncio.sleep(1)
        #print(num_members)

    print("Done")
    await vc.disconnect()

help_message = "Plays one of the following internet radio stations: (.radio [station])\nelectric\ntrancebase / technobase\norganlive (Organ Media Foundation)\nbaroque (Positively Baroque / OMF)\nexperience (The Organ Experience / OMF)"


