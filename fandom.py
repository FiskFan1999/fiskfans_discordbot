#Note: use
import unidecode
import discord
import asyncio
import requests
from html.parser import HTMLParser
import json

indexURL = "https://pipe-organ.fandom.com/wiki/Special:AllPages"

JSONPATH = "./examplesafe/fandomwiki.json"

webpageImages = []
def getWebpageImages(requesttext):
    global webpageImages
    webpageImages = []
    p = ImagesParser()
    p.feed(requesttext)

    images = []
    for im in webpageImages[:]:
        if im.startswith("https://static.wikia.nocookie.net/pipe-organ/images"):
            images.append(im)
    return images

class ImagesParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        global webpageImages
        if tag == "img":
            for a in attrs:
                if a[0] == "src":
                    webpageImages.append(a[1])

    def handle_endtag(self, tag):
        pass
    def handle_data(self, data):
        pass

isTitle = False
webpageTitle = None

def getPageTitle(requesttext):
    global isTitle
    global webpageTitle
    webpageTitle = None
    isTitle = False

    p = TitleParser()
    p.feed(requesttext)
    t = webpageTitle
    if t.endswith(" | Pipe Organ Wiki | Fandom"):
        t = t[:len(" | Pipe Organ Wiki | Fandom") * -1]
    return t

class TitleParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        global isTitle
        if tag == "title":
            isTitle = True
    def handle_endtag(self, tag):
        pass
    def handle_data(self, data):
        global webpageTitle
        if isTitle:
            #print(data)
            pass
        if isTitle and webpageTitle == None:
            webpageTitle = data.strip()


isParagraph = False
paragraphs = []
class WikiPageParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        global isParagraph
        if tag == "p": isParagraph = True
    def handle_endtag(self, tag):
        global isParagraph
        if tag == "p": 
            isParagraph = False
            paragraphs.append("")
    def handle_data(self, data):
        global paragraphs
        if isParagraph:
            ds = data.strip()
            if len(ds) == 0: return
            paragraphs[-1] += ds + " "

def getWikiPageParagraphs(requesttext):
    global paragraphs
    paragraphs = [""]
    pa = WikiPageParser()
    pa.feed(requesttext)
    return paragraphs[:]

class IndexPageParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        if tag != "a":
            return
        hasTitle = False
        title = None
        href = None
        for a in attrs:
            if a[0] == "title":
                hasTitle = True
                title = a[1]
            elif a[0] == "href":
                href = a[1]
        if hasTitle:
            ud = unidecode.unidecode(title)
            indexLinks[ud] = href

    def handle_endtag(self, tag):
        pass
    def handle_data(self, data):
        pass

def getSearchUrl(organName):
    url = "https://pipe-organ.fandom.com/wiki/Special:Search?query=%22" + organName.replace(" ", "+") + "%22"
    return url

def get_json(path):
    with open(path) as json_file:
        file_json = json.load(json_file)
    return file_json

async def doFandomCommand(client, message):
    indexLinks = get_json(JSONPATH)
    argv = message.content.split()
    
    if len(argv) == 1:
        await message.channel.send(".fandom (organ name)")
        return

    if argv[1] == "list":
        await message.channel.send("Direct-message you list of all fandom pages", delete_after=5.0)
        textBlocks = [""]
        keys = indexLinks.keys()
        for key in keys:
            if len(textBlocks[-1]) + len(key) > 1990:
                textBlocks.append("")
            textBlocks[-1] += "\n" + key

        for t in textBlocks:
            emb = discord.Embed(description="```" + t + "```")
            await message.author.send(embed=emb)

        return


    nameLink = []
    hrefLink = []
    namehref = dict()

    organName = " ".join(argv[1:])
    for link in indexLinks:
        if organName.lower() in link.lower():
            hrefLink.append(indexLinks[link])
            nameLink.append(link)
            namehref[link] = indexLinks[link]

    if len(hrefLink) == 0:
        await message.channel.send("No entry found")
        return

    if len(hrefLink) > 1:
        #Too many found
        await message.channel.send(namehref)
        #description = "\n".join(nameLink)
        description = ""
        for n in namehref:
            description += "[" + n + "](https://pipe-organ.fandom.com" + namehref[n] + ")\n"
        url = getSearchUrl(organName)
        embed = discord.Embed(title="Too many found", url=url, description=description[:2000])
        await message.channel.send(embed=embed)
        return

    #otherwise, it is only one long.

    href = "https://pipe-organ.fandom.com" + hrefLink[0]

    r = requests.get(href)

    paragraph_text = []
    for para in getWikiPageParagraphs(r.text):
        ps = para.strip()
        if len(ps) == 0:
            continue
        paragraph_text.append(ps)
    
    full_text = "\n\n".join(paragraph_text) #[:2048]
    if len(full_text) > 2048:
        full_text = full_text[:2045] + "..."

    pageTitle = getPageTitle(r.text)

    pageImages = getWebpageImages(r.text)

    embed = discord.Embed(title=pageTitle,description=full_text,url=href)

    if pageImages:
        embed.set_image(url=pageImages[0])

    await message.channel.send(embed=embed)
