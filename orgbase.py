import discord
import asyncio
import requests
import maketextfile
import textwrap
from html.parser import HTMLParser

WRAP_WIDTH = 107

def getWrappedText(text):
    paragraphs = []
    for paragraph in text.split("\n"):
        paragraphs.append(textwrap.fill(paragraph, width=WRAP_WIDTH))
    return "\n".join(paragraphs)

class ImageParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.Images = []
    def handle_starttag(self, tag, attrs):
        if tag == "img":
            for attr in attrs:
                if attr[0] == "src" and attr[1].lower().startswith("/orgbase"):
                    self.Images.append("http://orgbase.nl" + attr[1].replace(" ", "%20"))
    def handle_endtag(self, tag):
        pass
    def handle_data(self, data):
        pass

def getImages(htmltext):
    p = ImageParser()
    p.feed(htmltext)
    newImages = []
    return p.Images[:]

class OrgbaseTextParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.IsGettingText = False
        self.FinalText = []
    def handle_starttag(self, tag, attrs):
        if tag == "br":
            if len(self.FinalText) and self.FinalText[-1] != "\n":
                self.FinalText.append("\n")
    def handle_endtag(self, tag):
        pass
    def handle_data(self, data):
        if data.strip() == "Complete description of the selected organ":
            self.IsGettingText = True
            return
        if data.strip() and self.IsGettingText:
            self.FinalText.append(data.strip())

def getOrgbaseText(requesttext):
    parser = OrgbaseTextParser()
    parser.feed(requesttext)
    return parser.FinalText[:]

def getFormattedText(textlist):
    final = ""
    for t in textlist:
        if t == "\n":
            final += "\n"
        else:
            final += t + " "
    return final

def getOrgbaseUrl(idnum):
    prefix = "http://orgbase.nl/scripts/ogb.exe?database=ob2&%250="
    suffix = "&LGE=EN&LIJST=lang"
    return prefix + idnum + suffix

async def discordCommand(client, message):
    argv = message.content.split(" ")
    if len(argv) > 1 and argv[1] in ["q", "search"]:
        await message.channel.send("The bot does not have orgbase searching yet.\nPlease feel free to search for the organ you would like to see using the website's search functions at this page: http://orgbase.nl/index_en.html")
        return
    if len(argv) == 2:
        idnum = argv[1]
        url = getOrgbaseUrl(idnum)
        r = requests.get(url)
        text = r.text
        textlist = getOrgbaseText(text)
        formatText = getFormattedText(textlist)
        #await message.channel.send(formatText[:2000])
        #await message.channel.send(getImages(text))
        images = getImages(text)

        if len(formatText.strip()) == 0:
            await message.channel.send("It appears that an orgbase entry with id number " + idnum + " is not available.")
            return
        elif len(formatText) < 2048:
            emb = discord.Embed(title="orgbase id " + idnum, url=url, description=formatText)
            if images:
                emb.set_image(url=images[0])

            await message.channel.send(embed=emb)
            return
        else:
            f = maketextfile.maketextfile(getWrappedText(formatText))
            emb = discord.Embed(title="orgbase id " + idnum, url=url)
            if images:
                emb.set_image(url=images[0])

            await message.channel.send(embed=emb, file=f)

if __name__ == "__main__":
    url = "http://orgbase.nl/scripts/ogb.exe?database=ob2&%250=2007052&LGE=EN&LIJST=lang"
    r = requests.get(url)
    text = r.text

    f = getOrgbaseText(text)
    print (getFormattedText(f))
